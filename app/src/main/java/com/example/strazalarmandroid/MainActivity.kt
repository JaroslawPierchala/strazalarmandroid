package com.example.strazalarmandroid

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var signalRListener: SignalRListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val login = intent.getStringExtra("FiremanLogin")

//        hubConnection = HubConnectionBuilder.create("https://127.0.0.1:44399/firemanHub").build()


        signalRListener = SignalRListener.getInstance(
            noAlarm,
            alarm,
            textAlarm,
            timeET,
            accept_button,
            decline_button
        )

        signalRListener.startConnection(login)

//        hubConnection.on("ReceiveFireActionAlarm", {fireActionId, fireActionName ->
//            noAlarm.visibility = View.GONE
//            alarm.visibility = View.VISIBLE
//            textAlarm.text = "Alarm: $fireActionName"
//            loginET.visibility = View.VISIBLE
//            timeET.visibility = View.VISIBLE
//
//            fireActionIdAlarm = fireActionId
//
//            accept_button.visibility = View.VISIBLE
//            decline_button.visibility = View.VISIBLE
//        }, Int::class.java, String::class.java)

        accept_button.setOnClickListener {
            signalRListener.sendToServer(login.toString(), timeET.toString(), true)

            if (signalRListener.isConnected) {
                noAlarm.visibility = View.VISIBLE
                alarm.visibility = View.GONE
                timeET.visibility = View.GONE
                accept_button.visibility = View.GONE
                decline_button.visibility = View.GONE
            }
        }
        decline_button.setOnClickListener {
            signalRListener.sendToServer(login.toString(), timeET.toString(), false)

            if (signalRListener.isConnected) {
                noAlarm.visibility = View.VISIBLE
                alarm.visibility = View.GONE
                timeET.visibility = View.GONE
                accept_button.visibility = View.GONE
                decline_button.visibility = View.GONE
            }
        }
    }
}
