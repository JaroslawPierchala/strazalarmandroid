package com.example.strazalarmandroid;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;

public class SignalRListener {
    private static SignalRListener instance;

    private Integer fireActionIdAlarm = 0;

    private HubConnection hubConnection;

    public SignalRListener(ImageView noAlarm,
                           ImageView alarm,
                           TextView textAlarm,
                           EditText timeET,
                           Button accept_button,
                           Button decline_button) {

        hubConnection = HubConnectionBuilder.create("https://127.0.0.1:44399/firemanHub").build();

        hubConnection.on("ReceiveFireActionAlarm", (fireActionId, fireActionName) -> {
                noAlarm.setVisibility(View.GONE);
                alarm.setVisibility(View.VISIBLE);
                String id = "Alarm: " + fireActionId;
                textAlarm.setText(id);
                timeET.setVisibility(View.VISIBLE);

                fireActionIdAlarm = fireActionId;

                accept_button.setVisibility(View.VISIBLE);
                decline_button.setVisibility(View.VISIBLE);
        }, Integer.class, String.class);
    }

    public static SignalRListener getInstance(ImageView noAlarm,
                                              ImageView alarm,
                                              TextView textAlarm,
                                              EditText timeEt,
                                              Button accept_button,
                                              Button decline_button) {
        if (instance == null)
            instance = new SignalRListener(noAlarm,
                    alarm,
                    textAlarm,
                    timeEt,
                    accept_button,
                    decline_button);
        return instance;
    }
    public void startConnection(String login) {
        if (hubConnection.getConnectionState() == HubConnectionState.DISCONNECTED) {
            hubConnection.start();
            sendLoginToServer(login);
        }
    }
    public boolean isConnected() {
        if (hubConnection.getConnectionState() == HubConnectionState.CONNECTED)
            return true;
        Log.e("SignalR_Send", "connection:" + hubConnection.getConnectionState());
        return false;
    }
    public void sendToServer(String login, String estimatedTimeOfArrival, Boolean participation) {
        if ( hubConnection.getConnectionState() == HubConnectionState.CONNECTED)
            hubConnection.send("SendActionAnswer",
                    fireActionIdAlarm,
                    login,
                    estimatedTimeOfArrival,
                    participation);
        else
            Log.e("SignalR_Send", "connection:" + hubConnection.getConnectionState());
    }
    public void sendLoginToServer(String login)
    {
        if (hubConnection.getConnectionState() == HubConnectionState.CONNECTED)
            hubConnection.send("SendFireman", login);
        else
            Log.e("SignalR_Send", "connection:" + hubConnection.getConnectionState());
    }
}
